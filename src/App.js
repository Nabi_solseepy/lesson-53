import React, { Component } from 'react';

import Task from './component/Task'
import AddTaskForm from './component/AddTaskForm'

import './App.css';

class App extends Component {
  state = {
    currentTask: 3,
    tasks: [{task: 'Buy milk', id: 1},{task: 'Walk with dog', id: 2}, {task: 'Do homework', id: 3}],
      value:''
  };

  addTaskHandler = () => {
    let newTask = {task: this.state.value, id: this.state.currentTask + 1};
    let tasks = [...this.state.tasks];
     tasks.push(newTask);
    this.setState({tasks, value: '' , currentTask: this.state.currentTask +1 })

  };
  chengeHendler = (value) => {
    this.setState({value})
  };
  removeHendler = (task) => {
    let id = this.state.tasks.findIndex((item) => item.id === task);
    let tasks = [...this.state.tasks];
    tasks.splice(id,1);
    this.setState({tasks, currentTask: this.state.currentTask -1})
  }
  render() {
    return (
      <div className="App">
       <AddTaskForm value={this.state.value} chenged={this.chengeHendler} addTask={this.addTaskHandler}/>
          {this.state.tasks.map((element, id) =>  <Task id={element.id} removeTask={this.removeHendler} key={id}text={element.task}/>)}
      </div>
    );
  }
}

export default App;
