import React from  'react';
import './Task.css'

const Task = (props) =>{
    return(
        <div className="task">
         <p>{props.text}<span><button className="taskBtn" type="button" onClick={() => props.removeTask(props.id)}>x</button></span></p>
        </div>

    )
};


export default Task;