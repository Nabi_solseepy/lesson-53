import React from  'react';
import './AddTaskForm.css'

const AddTaskForm = (props) => {
    return(
        <div className="Item">
            <input onChange={(e) => props.chenged(e.target.value)} value={props.value} className="input" placeholder="write a message"/>
            <button onClick={props.addTask} className="btn">Add</button>
        </div>
    )
}

export default AddTaskForm
